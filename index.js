const express = require('express')
const app = express()
const PORT = 3000;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

let userDb = [
    {
        userName: "johndoe",
        password: "johndoe1234"
    },
]

//1
app.get("/home", (req, res) => res.status(202).send("Welcome to the Homepage"))

//3
app.get("/users", (req, res) => res.status(202).send(userDb))

//5
app.delete("/delete-users", (req, res) => {
    const {userName} = req.body

    console.log(userDb);
    for(let x = 0; x < userDb.length; x++){
        if (userName == userDb[x].userName){
            userDb.splice(x, 1);
            res.send(`User ${userName} has been deleted`)
            console.log(userDb);
            break;
        }
        else{
            res.send(`User not found`)
        }
    }
    
})

app.listen(PORT, () => console.log(`Server is connected to ${PORT}`))